//s23 Activity

//#3 --- insertOne method
db.rooms.insert(
   {
	name : "single",
    accomodates : 2,
    price : 1000,
    description: "A simple room with all the basic necessities",
    rooms_available: 10,
    isAvailable: "false"
	}
)


//#4 --- insertMany method
db.rooms.insertMany([
   {
	name : "double",
    accomodates : 3,
    price : 2000,
    description: "A room fit for a small family going on a vacation",
    rooms_available: 5,
    isAvailable: "false"
	},

   {
	name : "queen",
    accomodates : 4,
    price : 4000,
    description: "A room with a queen sized bed perfect for a simple getaway",
    rooms_available: 15,
    isAvailable: "false"
	}

])

//#5 find method -
db.rooms.find({
    
    name: "double"
    
    })

//#6 updateOne
db.rooms.updateOne(
		    { name: "queen" },
		    {
		        $set : {
		            accomodates : 4,
		            price : 4000,
		            description: "A room with a queen sized bed perfect for a simple getaway",
		            rooms_available: 0,
		            isAvailable: "false"
		        }
		    }
	)

//#7 deletoMany
db.rooms.deleteMany(
	{ rooms_available: 0 }


	)